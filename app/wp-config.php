<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpressMG');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&h|*C)8x]b:%+U<<U`+QHowzctr1inb9!-83dO+dT@=+7q-(U0KYumeV5M4iTo8]');
define('SECURE_AUTH_KEY',  '}c sMue92mbAZ:=uYJyZ[{[Z.rdT|!M_K|]-`4Oh/9&EJ2vlHlpqB-S_xDA?(7a*');
define('LOGGED_IN_KEY',    '9;9&$(pnFIoMv?e:ellaO/wPT#X87];f^cZ*e?z;UgDdmRn6fjZL+vU#K.F|m+&-');
define('NONCE_KEY',        '|kRw*!d!+/qW,u5NTvbvd3Dc,>+7|!6Ay?EcFCP<Mr_KQa]P#cHLxCmH%TaDn^?$');
define('AUTH_SALT',        '@La`BeqRceBB&f+vGv(]QT0n%Q>>(HRPeum&iBdA2dvPhUlF+-(}W(=zIK@j+b|G');
define('SECURE_AUTH_SALT', 's@4fe|Ul9%bNs.>kSL;Wim?r}`o{D*G A1-+|,aG[[n|c-a}|XjHCk=sQOw<O$Ze');
define('LOGGED_IN_SALT',   'zdB$443-bhXt_</Snk9;-tx8+_jD5Xt+*Yw{Z4d9KK@N-4~~_|Q-AE+YO`+B!C8D');
define('NONCE_SALT',       'P@&lM7mgs#-=5k2|xg-/+?ZFd1]|Mn0e2a?+e^3C,7NAl} FHh)W@k+1JL|6z3gG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
if (WP_DEBUG) {
	define('WP_DEBUG_LOG', true);
	define('WP_DEBUG_DISPLAY', true);
	@ini_set('display_errors', 0);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
