<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage Kaplan
 */
get_header(); ?>


    <div class="row">

        <div class="col-xs-12 col-sm-8">

            <?php
            if ( have_posts() ) : ?>

                <?php if ( is_home() && ! is_front_page() ) : ?>
                    <header>
                        <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                    </header>
                <?php endif; ?>

                <?php

                while ( have_posts() ) :
                    the_post();
                    //var_dump(get_post_format());
                    ?>

                    <h3><?= the_title(); ?></h3><?php
                    the_time();
                    the_category();
                    the_content();
                    ?><hr><?php
                    // End the loop.
                endwhile;

                // Previous/next page navigation.
                the_posts_pagination( array(
                    'prev_text'          => __( 'Previous page', 'kaplantheme' ),
                    'next_text'          => __( 'Next page', 'kaplantheme' ),
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'kaplantheme' ) . ' </span>',
                ) );

            // If no content, include the "No posts found" template.
            else :
                get_template_part( 'content', 'none' );

            endif;
                ?>

        </div>

        <div class="col-xs-12 col-sm-4">
            <?php get_sidebar(); ?>
        </div>

    </div>

<?php get_footer(); ?>
