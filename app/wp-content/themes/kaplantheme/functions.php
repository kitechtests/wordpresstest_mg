<?php
/**
 * Created by PhpStorm.
 * User: moises
 * standart functions file for kaplan theme
 */


/**
 * enqueue scripts function
 */
function kaplanThemeEnqueue()
{
    $bootstrap_dist = 'bootstrap-3.3.5-dist';
    $font_aweosome_dist = 'font-awesome-4.4.0';

    wp_enqueue_style('bootstrap',  get_template_directory_uri().'/assets/'.$bootstrap_dist.'/css/bootstrap.min.css', [], '3.3.5', 'all');
    wp_enqueue_style('fawesome',  get_template_directory_uri().'/assets/'.$font_aweosome_dist.'/css/font-awesome.min.css', [], '4.4.0', 'all');
    wp_enqueue_style('templateFreelance',  get_template_directory_uri().'/assets/css/templateFreelance.css', [], '0.1.0', 'all');
    wp_enqueue_style('kaplanstyleA',  get_template_directory_uri().'/assets/css/kaplan.css', [], '0.1.0', 'all');

    //wp_enqueue_script("jquery");
    wp_enqueue_script('kaplanstylejs', get_template_directory_uri().'/assets/js/kaplan.js',['jquery'], '0.1.0', true);
    wp_enqueue_script('bootstrapjs', get_template_directory_uri().'/assets/'.$bootstrap_dist.'/js/bootstrap.min.js',['jquery'], '0.1.0', true);
}

/**
 *  load actions function
 */
function kaplanThemeActions()
{
    add_action('wp_enqueue_scripts', 'kaplanThemeEnqueue');
    add_action('init','kaplanThemeSupport');
    add_action('widgets_init','kaplanThemeSidebars');
}

/**
 *  load theme suport like menus, backgrounds and others
 */
function kaplanThemeSupport()
{
    add_theme_support('menus');
    add_theme_support('custom-background');
    add_theme_support('automatic-feed-links');
    add_theme_support('custom-header');
    //post formats support
    add_theme_support('post-formats', ['image', 'video', 'aside']);
}

/**
 *  load theme widgets sidebars
 */
function kaplanThemeSidebars()
{
    register_sidebar([
            'name'          => 'Sidebar',
            'id'            => 'sidebar-1',
            'class'         => 'sidebar1',
            'description'   => 'Simple Sidebar',
            //from wordpress Codex
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h1 class="widget-title">',
            'after_title'   => '</h1>'
        ]
    );
    register_sidebar([
            'name'          => 'Sidebar',
            'id'            => 'sidebar-2',
            'class'         => 'sidebar2',
            'description'   => 'Simple Sidebar',
            //from wordpress Codex
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h1 class="widget-title">',
            'after_title'   => '</h1>'
        ]
    );
}
/**
 *  register
 */
function kaplanRegister()
{
    register_nav_menu('header_menu','Primary header top menu navigation');

}
kaplanRegister();
kaplanThemeActions();

