<!<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Moises Kaplan Task</title>
    <?= wp_head(); ?>
</head>
<?php
//is_home not valid home is not front_page
if(is_front_page()){
        $class = ['home-class'];
    } else {
        $class = ['general-class'];
    }
?>

<body <?= body_class($class); ?>>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <nav class="navbar navbar-inverse ">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#page-top"><?= wp_get_theme()->get( 'Name' ); ?></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" aria-expanded="false" style="height: 1px;">
                        <?php wp_nav_menu(
                            [
                                'theme_location' => 'header_menu',
                                'container' => false,
                                'menu_class' => 'nav navbar-nav navbar-left'
                            ]
                        ); ?>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>
</div>



<div class="container">






<img src="<?= header_image()?>" height="<?= get_custom_header()->height ?>" with="<?= get_custom_header()->width; ?>" />

<!-- Main jumbotron for a primary marketing message or call to action
<div class="jumbotron">
    <div class="container">
        <h1>Hello, world!</h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p>
    </div>
</div>

-->
