<?php
/**
 * Template Name: Provider Video PlayList
 *
 * @package WordPress
 * @subpackage Kaplan
 */
get_header(); ?>
<div class="container">
            <?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                    //var_dump(get_page_template_slug( $post->ID ));
                    get_template_part( 'content', 'videoPlaylist' );
                endwhile;

                the_posts_pagination( array(
                    'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                    'next_text'          => __( 'Next page', 'twentyfifteen' ),
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                ) );
            else :
                get_template_part( 'content', 'none' );
            endif;
            ?>
</div>
<?php get_footer(); ?>
