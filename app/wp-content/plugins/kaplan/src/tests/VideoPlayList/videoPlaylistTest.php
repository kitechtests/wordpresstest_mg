<?php

use VideoPlayList\providers\VideoPlayListProviderYoutube;

class VideoPlayListTest extends PHPUnit_Framework_TestCase {

    public function testFeedService()
    {
        /**
         * Reflection class for acessing private properties and/or methods
         * @todo test with Closure::bind(), most results show speed increase
         * @todo also use reflection class to access private methods.
         */

        $serviceCall = new VideoPlayListProviderYoutube();

        //get array in feed ?
        $this->assertTrue(is_array($serviceCall->VideoPlayListGetter()));

        //200 service is OK
        $this->assertTrue($serviceCall->VideoPlayListGetter()['status'] == 200);

        $serviceCall->serviceFields['key'] = 'nokey';

        //force non 200
        $this->assertTrue($serviceCall->VideoPlayListGetter()['status'] != 200);


    }

}
