<?php namespace VideoPlayListApp\models;
/**
 * Created by PhpStorm.
 * User: moises
 */
use VideoPlayList\src\GetPayListDataController;

class youtubePlaylistModel
{
    /**
     * @param $params
     * @return array
     * @throws \Exception
     */
    public function outputData($params = [])
    {
        if (!empty($params)) {
            $feed = GetPayListDataController::providerFactory('youtube', $params['videonumber']);
            return $feed['data'];
        }
        return [];

    }
}
