<?php
/**
 * @author Moisés Gaspar <moisesgaspar22@gmail.com>
 */

// Looking for the AJAX feed ?
// go to console and console.log(dataObject)

if (isset($data['data']['items'])) {
?>
    <section id="portfolio">
        <div class="container">
            <div class="row">
            <?php
                //image size
                //these values are in ['snippet']['thumbnails']['medium']['width'] and ['height']
                //but for consistency are fixed inline style
                foreach($data['data']['items'] as $video) {
                    if (isset($video['snippet']['thumbnails'])) {
                        $thumb = $video['snippet']['thumbnails']['medium']['url'];

                    } else {
                        $thumb =  plugins_url( '/images/nothumbnail.jpg', dirname(__FILE__) );
                    }
                    ?>
                    <div class="">
                        <a href="#<?= $video['id'] ?>" class="portfolio-link" data-toggle="modal">
                            <img src="<?= $thumb ?>" class="" style="width: 100px; height: 50px;" alt="" ">
                            <p style="font-size: 10px !important"><?= $video['snippet']['title'] ?></p>
                        </a>
                    </div>
                <?php
                }
            ?>
            </div>
        </div>
    </section>

<!-- Video Modals -->

    <?php
    foreach($data['data']['items'] as $video) {
        ?>
        <div class="portfolio-modal modal fade" id="<?= $video['id'] ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h3><?= $video['snippet']['title'] ?></h3>
                                <hr class="star-primary">
                                <iframe width="560" height="315" src="<?= $video['videoUrlEmbed'] ?>" frameborder="0"
                                        allowfullscreen></iframe>
                                <h4>Published at: <?php
                                    $time = strtotime($video['snippet']['publishedAt']);
                                    $newformat = date('Y-m-d h:m', $time);
                                    echo $newformat;
                                    ?>
                                </h4>

                                <div class="video_description"><p> <?= $video['snippet']['description'] ?> </p></div>
                                <ul class="list-inline item-details">
                                    <li>View on Youtube:
                                        <strong><a href="<?= $video['videoUrl'] ?>" target="_blank">Youtube</a>
                                        </strong>
                                    </li>
                                    <li>Date:
                                        <strong><?= $newformat ?></a>
                                        </strong>
                                    </li>
                                </ul>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                        class="fa fa-times"></i> Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
} else {
    echo "Sorry Data not found";
}?>
