<?php namespace VideoPlayListApp\core;
/**
 *@todo Create a service container for deal with the class dependencies
 */

use VideoPlayListApp\controllers\videolistController;
use VideoPlayListApp\pub\videoPlaylistinit;
use VideoPlayListApp\widget\videoPlayListWidget;


class app
{
    public static function start()
    {
        self::register_actions();
    }
    public static function register_shortcodes(){
        add_shortcode( 'video_play_list_provider', [new videolistController(), 'videolistControllerLoader'] );
        new videoPlaylistinit();
    }
    public static function register_actions(){
        add_action( 'init', [new self(),'register_shortcodes']);
        add_action('widgets_init', [new videoPlayListWidget(), 'register_this_widget']);

    }
}


