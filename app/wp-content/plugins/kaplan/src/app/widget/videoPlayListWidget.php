<?php namespace VideoPlayListApp\widget;

/**
 * Class videoPlayListWidget
 * @package VideoPlayListApp\widget
 */
use VideoPlayList\src\GetPayListDataController;
use VideoPlayListApp\controllers\videolistController;


class videoPlayListWidget extends \WP_Widget
{

    public $name = 'Video_PLay_list';
    public $description = 'Video PLay List';


    /**
     * List all controllable options here along with a
     * default value.
     * The values can be distinct for each instance of the widget.
     */
    public $control_options = array();


    /**
     * Constructor
     * parent initiator
     */
    function __construct() {
        //@todo translations
        // load_plugin_textdomain( 'kaplan' );

        parent::__construct(
            'Video_play_list',
            __( 'Video Playlist Widget' , 'kaplan'),
            array( 'description' => __( 'Display a video playlist' , 'kaplan') )
        );

        if ( is_active_widget( false, false, $this->id_base ) ) {
            add_action( 'wp_head', array( $this, 'css' ) );
        }
    }

    function css() {
        ?>

        <style type="text/css">
            .a-stats {
                width: auto;
            }

        </style>

        <?php
    }

    function form( $instance ) {
        if ( $instance ) {
            $title = $instance['title'];
        }
        else {
            $title = __( 'Nº Videos' , 'kaplan');
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Nº de Videos:' , 'kaplan'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>

        <?php
    }

    function update( $new_instance, $old_instance ) {
        $instance['title'] = strip_tags( $new_instance['title'] );
        return $instance;
    }

    function widget( $args, $instance ) {

        $feed = GetPayListDataController::providerFactory('youtube', $instance['title']);
        $output = new videolistController();
        ?>

        <div class="a-stats">
            <a href="video-playlist"  title=""><?php echo __('Videos Playlist', 'kaplan') ?>
                <?= $output->widgetVideolistControllerLoader($feed['data']); ?>
            </a>

        </div>

        <?php
        echo $args['after_widget'];
    }
    /**
     * register
     */
    public static function register_this_widget()
    {
        register_widget(__CLASS__);
    }


}
