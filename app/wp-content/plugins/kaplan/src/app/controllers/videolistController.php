<?php namespace VideoPlayListApp\controllers;


class videolistController extends controller
{
    public function videolistControllerLoader( $params )
    {
        // Load Model
        $homeM = $this->model('youtubePlaylistModel');
        //Load View
        $this->view('youtubePlaylistView', ['data' => $homeM->outputData($params)]);
    }

    public function widgetVideolistControllerLoader( $params )
    {
        //Load View
        //I'm bypassing the model because I allready have the feed!
        $this->view('widgetYoutubePlaylistView', ['data' => $params]);
    }
}

