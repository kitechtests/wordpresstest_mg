<?php namespace VideoPlayListApp\controllers;
/**
 * Created by PhpStorm.
 * User: moises
 */

use VideoPlayListApp\models\youtubePlaylistModel;

class controller
{
    /**
     * @var string
     */
    private $model_path;

    /**
     * @var
     */
    private $view_path;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->model_path = dirname(__FILE__).'/../models/';
        $this->view_path = dirname(__FILE__).'/../views/';
    }
    /**
     * @param $local
     * @param $file
     * @param $ext
     * @return int
     */
    private function fileExists($local, $file, $ext)
    {
        return $exists = file_exists($local . $file . $ext) ? true : false;
    }
    /**
     *
     * @param string $modelName
     * @return \modelName
     */
    public function model($modelName ='', $param =[])
    {
        //Because you are using namespaces you need to fully qualify here
        $class = '\\VideoPlayListApp\\models\\'.$modelName;
        return new $class($param);

    }
    /**
     *
     * @param type $viewName
     * @param type $data
     */
    public function view($viewName, $data = [])
    {
        require_once $this->view_path . $viewName . '.php';

    }
}
