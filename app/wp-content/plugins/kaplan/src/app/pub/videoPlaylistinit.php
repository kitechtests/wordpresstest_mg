<?php namespace VideoPlayListApp\pub;

/**
 * @author  Moises Gaspar <moisesgaspar22@gmail.com>
 */


class videoPlaylistinit
{
    /**
     * @var string
     */
    private $plugin_js_url;
    /**
     * @var string
     */
    private $plugin_css_url;

    /**
     * constructor
     */
    function __construct()
    {
        add_filter( 'Video_Play_List_instance', [ $this, 'get_instance' ] );

        $this->plugin_js_url = plugins_url( '/pub/js/', dirname(__FILE__) );
        $this->plugin_css_url = plugins_url( '/pub/css/', dirname(__FILE__) );
        $this->enqueue_script();
    }

    /**
     * Just enqueue_script local and modernizr is not done yet
     */
    private function enqueue_script()
    {
        wp_enqueue_style( 'local_css', $this->plugin_css_url . 'local.css', [], '0.1.0', 'all');

        if (wp_script_is( 'modernizr.custom.js', 'done' )) {
            return;
        } else {
            wp_enqueue_script( 'modernizr.custom.js' , $this->plugin_js_url . 'modernizr.custom.js', ['jquery'], true );
        }

        wp_register_script( 'video_local_js', $this->plugin_js_url . 'local.js');

        // you need to translate the array so Javascript can use it
        $translation_array = [
            'record_number' =>1
        ];

        //call the javascript and giv it the translation array
        wp_localize_script( 'video_local_js', 'bridge', $translation_array );
        //it will use jquery, yes
        wp_enqueue_script( 'video_local_js', $this->plugin_js_url . 'local.js', ['jquery'] , true);
    }

    /**
     * @return $this
     */
    public function get_instance()
    {
        return $this; // return the object
    }
  }// END OFF CLASS
