<?php namespace VideoPlayList\src;

use VideoPlayList\providers\VideoPlayListProvider;

/**
 * Manage providers and manage data response
 * Class GetUserCountryByIp
 * @package VideoPlayList\src
 */

Class GetPayListDataController
{
    /**
     * providers list and location
     * @var array
     */
    private static $provider = [
        'youtube' => 'VideoPlayList\\providers\\VideoPlayListProviderYoutube'
    ];


    /**
     * unsure that the given object is from the type VideoPlayListProvider
     * @param VideoPlayListProvider $provider
     */
    private static function setProvider(VideoPlayListProvider $provider) {
        self::$provider = $provider;
    }

    /**
     * Factory instantiator for the provider
     * Instantiate the provider and get the provider service data
     * Manage response and data output
     * @param string $provider
     * @return array
     * @throws \Exception
     */
    public static function providerFactory($provider = '', $videoNumber)
    {
        //@todo Factory design pattern with singleton for configuration set
        if (array_key_exists($provider, self::$provider)) {
            $class = self::$provider[$provider];
        } else {
            throw new \Exception('Provider name not found!');
        }

        if (class_exists($class)) {
            self::setProvider(new $class($videoNumber));
            $resp = self::$provider->VideoPlayListGetter();
        } else {
            throw new \Exception("Unable to load {$class} provider!");
        }

        if ($resp['status'] == 200 and is_array($resp['data'])) {
            return [
                'status' => 200,
                'data' => $resp['data']
            ];
        } else{
            return [
                'status' => $resp['status'],
                'data' => $resp['data']
            ];
        }
    }
} //End Class
