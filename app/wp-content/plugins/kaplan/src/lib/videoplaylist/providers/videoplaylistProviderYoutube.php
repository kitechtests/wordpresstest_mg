<?php namespace VideoPlayList\providers;

/**
 * Class VideoPlayListProviderGeobytes
 * Provider for the VideoPlayList service for geobytes
 * more @ www.geobites.com
 * @package VideoPlayList\providers
 * @author Moises <moisesgaspar22@gmail.com>
 */

class VideoPlayListProviderYoutube extends VideoPlayListProvider
{
    /**
     * Provider service URL
     * @var string
     */
    private $serviceURL  = 'https://www.googleapis.com/youtube/v3/playlistItems?';

    /**
     * Returned array data with given dictionary
     * @var array
     */
    private $serviceData = [];

    /**
     * Provider fields
     * @var array
     */
    public $serviceFields = [
        'part'       => 'snippet',
        'order'      => 'date',
        'playlistId' => 'PLdYcdVQDHNRK0eyAwBDm8EiyLrnStj0Ae',
        'key'        => 'AIzaSyAhDYYnDwphzM-DjwpvwYg43KEX39TBKAQ',
        'pageToken'  => '',
        // max records returned in a single call is 50
        // maxResults holds the number of records wanted
        'maxResults' => 50
    ];

    /**
     * url to direct view on Youtube
     * @var string
     */
    private $playVideoLink = 'https://www.youtube.com/watch?v=';

    /**
     * url to embedded video on Modal
     * @var string
     */
    private $playVideoEmbed = 'https://www.youtube.com/embed/';



    public function __construct($numberVideos = false)
    {
        if ($numberVideos && is_numeric($numberVideos)) {
            $this->serviceFields['maxResults'] = $numberVideos;
        }
    }
    /**
     * adds a video url to embed and play the video directly
     * @param $data
     * @return mixed
     */
    private function buildNewStructure($data)
    {
        if (isset($data['data']['items'])) {
            foreach ($data['data']['items'] as &$video) {
                $video['videoUrl'] = $this->playVideoLink . $video['snippet']['resourceId']['videoId'];
                $video['videoUrlEmbed'] = $this->playVideoEmbed . $video['snippet']['resourceId']['videoId'];
            }
        }
        return $data;
    }

    /**
     * Builds the service Url with the given service fields
     * @return string
     */
    private function buildUrl()
    {
        $url = $this->serviceURL;

        foreach($this->serviceFields as $key => $value) {
           $url .= $key.'='.$value.'&';
        }

        return rtrim($url, "&") ;
    }

    /**
     * Gets the raw data drom the provider and returns the expected data
     * @return array
     */
    public function VideoPlayListGetter ()
    {
        $curl = curl_init();
        curl_setopt_array($curl,
            [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $this->buildUrl()
            ]
        );

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if($httpCode !== 200) {
            $this->serviceData = [
                'status' => $httpCode,
                'data'   => $httpCode.' error'
            ];
        } else {
            //This is returning a array not a object!
            $this->serviceData = [
                'status' => 200,
                'data'   => json_decode($response, true)
            ];
        }

        curl_close($curl);

        return $this->setResponse($this->buildNewStructure($this->serviceData), $this->dictionary);
    }
}
