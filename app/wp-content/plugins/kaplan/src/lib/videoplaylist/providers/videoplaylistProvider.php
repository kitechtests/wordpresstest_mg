<?php namespace VideoPlayList\providers;

/**
 * Class VideoPlayListProvider
 * @package VideoPlayList\providers
 * @author Moises <moisesgaspar22@gmail.com>
 * @todo implement raw data response
 */

abstract class VideoPlayListProvider
{
    /**
     * @var array retains the wanted response structure.
     */

    protected $dictionary = [
        'kind' => '',
        'etag' => '',
        'next_page_token' => '',
        'prev_page_token' => '',
        'page_info' => [
            'total_results'   => '',
            'results_perpage' => ''
        ],
        'items' => [
            'kind' => '',
            'etag' => '',
            'id'   => '',
            'snipset' => [
                'published_date' => '',
                'channel_id'     => '',
                'title'          => '',
                'description'    => '',
                'thumbnails'     => [
                    'default' => [
                        'url'    => '',
                        'width'  => '',
                        'height' => ''
                    ],
                    'medium' => [
                        'url'    => '',
                        'width'  => '',
                        'height' => ''
                    ],
                    'high' => [
                        'url'    => '',
                        'width'  => '',
                        'height' => ''
                    ]
                ],
                'channel_title' => '',
                'playlist_id'   => '',
                'position'      => '',
                'resource_id'   => [
                    'kind'     => '',
                    'video_id' => ''
                ]
            ]
        ]
    ];




    /**
     * constructor
     * @param string $userIp
     */
    public function __construct( )
    {
        // void
    }

    /**
     * Fill the response data array with the given service data respecting the dictionary structure
     * expects the data and a instance of the dictionary
     * @param array $data
     * @param $dictionary
     * @return array
     */
    final protected function setResponse( $data = [],  $dictionary  )
    {
        if (!is_array($data['data'])) {
            return $data;
        }
        //@todo compare data given from provider against the dictionary
        return $data;
    }

    /**
     * Sanitize the given provider data and sanitize it before returning it
     * @param string $var
     * @return mixed|string
     */
    protected static function sanitizeVar($var = '')
    {
        $var = strip_tags($var);
        //Allows only a-z A-Z 0-1 space . - , º
        $var = preg_replace('/[^a-zA-Z0-9\s\.\-\,\º\+]/', '', $var);

        return $var;
    }

}
