<?php namespace VideoPlayList\strategy;

use VideoPlayList\providers\VideoPlayListProvider;
/**
 * Created by PhpStorm.
 * User: moises
 */

/**
 * Class settingsStrategy
 * Singleton strategy class that returns the data to the library taking in consideration
 * the existence of a admin panel and a admin settings like
 * url data for the different providers (key, token, credentials, validation, etc)
 *
 * HELPER CLASS
 *
 */
class settings
{
    /**
     * @var $instance Object
     */
    public static $instance;

    /**
     * @var array
     */
    private  $settings = [];

    /**
     * @var type VideoPlayListProvider object
     */
    private $provider;

    /**
     * @param VideoPlayListProvider $provider
     * returns a single instance
     */
    public static function instance(VideoPlayListProvider $provider)
    {
      if(!isset(self::$instance)) {
          self::$instance = new settings($provider);
      }
    }

    /**
     * @param $provider constructor
     */
    private function __construct($provider)
    {
        $this->provider = $provider;
    }

    /**
     * Settings setter
     * strategy function to get provider settings
     */
    private function setSettings()
    {
        // read from environment to the given provider
        // detect if for ex. get_option('plugin_admin_name')['settings']
        // if exist means that the settings should be reade from the admin panel
        // and bypass the provider controller settings

        $settings = get_option('plugin_admin_name')['settings'];

        if($settings && is_array($settings)) {
            $this->settings = get_option('plugin_admin_name')['settings'];
        } else {
            $this->settings = $this->provider->serviceFields;
        }

    }

    /**
     * Settings getter
     * @return array
     */
    public function settings()
    {
        $this->setSettings();
        return $this->settings;
    }
}
