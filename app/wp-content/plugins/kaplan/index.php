<?php
/**
 * Plugin Name: Kaplan Video List Provider
 * Plugin URI: http://www.kaplan.com
 * Description: Plugin layer to VideoLIst provideres
 * Version: 0.1.0 alpha
 * Author: Moises <moisesgaspar22@gmail.com>
 * Author URI: https://wordpress.org/
 */


use VideoPlayListApp\core\app;
use VideoPlayListApp\widget\videoPlayListWidget;

/**
 * To use this plugin use [[video_play_list_provider videoNumber=50]] macro in any post or page
 * Video number is the number of videos on the feed - max is 50
 */

require (dirname(__FILE__).'/src/vendor/autoload.php');

app::start();



